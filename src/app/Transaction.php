<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'device_name',
        'retail_name',
        'price',
        'number'
    ];
}
