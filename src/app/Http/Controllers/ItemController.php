<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * 一覧表示
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request){
      $items = Item::orderBy('created_at', 'desc')->get();

      return view('items.index', [
        'items' => $items
      ]);
    }
}
