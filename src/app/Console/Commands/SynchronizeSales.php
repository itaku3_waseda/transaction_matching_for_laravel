<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SynchronizeSales extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $transactions = DB::table('transactions')->get();

        foreach ($transactions as $transaction)
        {
            $overlap = 0;
            $sales = DB::table('sales')->get(); 
            foreach ($sales as $sale)
            {
                if($sale->transaction_id == $transaction->id)
                {
                    $overlap = 1;
                }
            }

            if ($overlap == 0)
            {
                DB::table('sales')->insert
                ([
                    'transaction_id'    => $transaction->id,
                    'device_name'       => $transaction->device_name,
                    'retail_name'       => $transaction->retail_name,
                    'price'             => $transaction->price,
                    'number'            => $transaction->number,
                    'created_at' => now()
                ]);
            }
        }
    }
}
