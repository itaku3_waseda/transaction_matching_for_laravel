<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


class TransactionMatching extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:matching';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $transactions = DB::table('transactions')->get();
        $invoices     = DB::table('invoices')->get();


        //ここまででディスカウントデータをmatching_tableに用意
        $checker = 0;
        $overlap = 0;
        foreach ($invoices as $invoice)
        {
            $matching_table = DB::table('matching')->get();
            foreach ($transactions as $transaction)
            {
                if ($invoice->device_name == $transaction->device_name) $checker = $checker+1;
                if ($invoice->retail_name == $transaction->retail_name) $checker = $checker+1;
                if ($invoice->price       == $transaction->price)       $checker = $checker+1;
                if ($invoice->number      == $transaction->number)      $checker = $checker+1;
                
                if ($checker==4)
                {
                    foreach ($matching_table as $matching_data)
                    {
                        if($matching_data->transaction_id == $transaction->id 
                          ||$matching_data->invoice_id == $invoice->id)
                        {
                            $overlap = 1;
                        }
                    }

                    if ($overlap == 0)
                    {
                        DB::table('matching')->insert
                        ([
                            'transaction_id'    => $transaction->id,
                            'invoice_id'        => $invoice->id,
                            'device_name'       => $invoice->device_name,
                            'retail_name'       => $invoice->retail_name,
                            'price'             => $invoice->price,
                            'number'            => $invoice->number,
                            'discount'          => $invoice->discount,
                            'actual_price'      => $invoice->price-$invoice->discount,
                            'created_at'        => now()
                        ]);
                        $overlap = 0;
                        $checker = 0;
                        break;
                    }
                    $overlap = 0;
                }
                $checker = 0;
            }
        }

        //sales_tableを更新
        $sales          = DB::table('sales')->get();
        $matching_table = DB::table('matching')->get();

        foreach ($matching_table as $matching_data)
        {
            DB::table('sales')->where('transaction_id', $matching_data->transaction_id)->update
            ([
                'price' => $matching_data->actual_price,
                'updated_at' => now()
            ]);
        }
    }
}







