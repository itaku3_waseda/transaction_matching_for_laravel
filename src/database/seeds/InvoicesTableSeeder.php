<?php

use Illuminate\Database\Seeder;

class InvoicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('invoices')->insert([
            'device_name' => 'iPhone11',
            'retail_name' => 'docomo',
            'price' => '82000',
            'number' => '1200',
            'discount' => '23200',
            'created_at' => now()
        ]);
        DB::table('invoices')->insert([
            'device_name' => 'iPhone11 Pro',
            'retail_name' => 'docomo',
            'price' => '120000',
            'number' => '900',
            'discount' => '21200',
            'created_at' => now()
        ]);

        DB::table('invoices')->insert([
            'device_name' => 'iPhone11',
            'retail_name' => 'au',
            'price' => '82000',
            'number' => '1200',
            'discount' => '23200',
            'created_at' => now()
        ]);
        DB::table('invoices')->insert([
            'device_name' => 'iPhone11 Pro',
            'retail_name' => 'au',
            'price' => '120000',
            'number' => '900',
            'discount' => '21200',
            'created_at' => now()
        ]);

        DB::table('invoices')->insert([
            'device_name' => 'iPhone11',
            'retail_name' => 'SoftBank',
            'price' => '82000',
            'number' => '1200',
            'discount' => '23200',
            'created_at' => now()
        ]);
        DB::table('invoices')->insert([
            'device_name' => 'iPhone11 Pro',
            'retail_name' => 'SoftBank',
            'price' => '120000',
            'number' => '900',
            'discount' => '21200',
            'created_at' => now()
        ]);

        DB::table('invoices')->insert([
            'device_name' => 'Pixel 4',
            'retail_name' => 'Google',
            'price' => '90000',
            'number' => '2500',
            'discount' => '16000',
            'created_at' => now()
        ]);
        DB::table('invoices')->insert([
            'device_name' => 'Pixel 4 XL',
            'retail_name' => 'Google',
            'price' => '110000',
            'number' => '1000',
            'discount' => '16000',
            'created_at' => now()
        ]);


        //再注文
        DB::table('invoices')->insert([
            'device_name' => 'iPhone11',
            'retail_name' => 'docomo',
            'price' => '82000',
            'number' => '1200',
            'discount' => '23200',
            'created_at' => now()
        ]);
        DB::table('invoices')->insert([
            'device_name' => 'iPhone11 Pro',
            'retail_name' => 'docomo',
            'price' => '120000',
            'number' => '900',
            'discount' => '21200',
            'created_at' => now()
        ]);

        DB::table('invoices')->insert([
            'device_name' => 'iPhone11',
            'retail_name' => 'Apple',
            'price' => '82000',
            'number' => '1200',
            'discount' => '23200',
            'created_at' => now()
        ]);
        DB::table('invoices')->insert([
            'device_name' => 'iPhone11 Pro',
            'retail_name' => 'Apple',
            'price' => '120000',
            'number' => '900',
            'discount' => '21200',
            'created_at' => now()
        ]);

        DB::table('invoices')->insert([
            'device_name' => 'Pixel 4',
            'retail_name' => 'au',
            'price' => '90000',
            'number' => '2500',
            'discount' => '16000',
            'created_at' => now()
        ]);
        DB::table('invoices')->insert([
            'device_name' => 'Pixel 4 XL',
            'retail_name' => 'au',
            'price' => '110000',
            'number' => '1000',
            'discount' => '16000',
            'created_at' => now()
        ]);

        DB::table('invoices')->insert([
            'device_name' => 'Pixel 4',
            'retail_name' => 'SoftBank',
            'price' => '90000',
            'number' => '2500',
            'discount' => '16000',
            'created_at' => now()
        ]);
        DB::table('invoices')->insert([
            'device_name' => 'Pixel 4 XL',
            'retail_name' => 'SoftBank',
            'price' => '110000',
            'number' => '1000',
            'discount' => '16000',
            'created_at' => now()
        ]);

    }
}
