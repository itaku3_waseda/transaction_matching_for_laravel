<?php

use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ここからApple
        DB::table('transactions')->insert([
            'device_name' => 'iPhoneX',
            'retail_name' => 'Apple',
            'price' => '120000',
            'number' => '2000',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'iPhone8',
            'retail_name' => 'Apple',
            'price' => '80000',
            'number' => '800',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'iPhone8 Plus',
            'retail_name' => 'Apple',
            'price' => '100000',
            'number' => '500',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'iPhoneXS',
            'retail_name' => 'Apple',
            'price' => '120000',
            'number' => '800',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'iPhoneXS MAX',
            'retail_name' => 'Apple',
            'price' => '150000',
            'number' => '300',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'iPhone11',
            'retail_name' => 'Apple',
            'price' => '82000',
            'number' => '1200',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'iPhone11 Pro',
            'retail_name' => 'Apple',
            'price' => '120000',
            'number' => '900',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'iPhone11 Pro MAX',
            'retail_name' => 'Apple',
            'price' => '150000',
            'number' => '500',
            'created_at' => now()
        ]);

        DB::table('transactions')->insert([
            'device_name' => 'iPhone11',
            'retail_name' => 'docomo',
            'price' => '82000',
            'number' => '1200',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'iPhone11 Pro',
            'retail_name' => 'docomo',
            'price' => '120000',
            'number' => '900',
            'created_at' => now()
        ]);

        DB::table('transactions')->insert([
            'device_name' => 'iPhone11',
            'retail_name' => 'au',
            'price' => '82000',
            'number' => '1200',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'iPhone11 Pro',
            'retail_name' => 'au',
            'price' => '120000',
            'number' => '900',
            'created_at' => now()
        ]);

        DB::table('transactions')->insert([
            'device_name' => 'iPhone11',
            'retail_name' => 'SoftBank',
            'price' => '82000',
            'number' => '1200',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'iPhone11 Pro',
            'retail_name' => 'SoftBank',
            'price' => '120000',
            'number' => '900',
            'created_at' => now()
        ]);
        
       	//ここからSamsung
        DB::table('transactions')->insert([
            'device_name' => 'Galaxy S10',
            'retail_name' => 'Samsung',
            'price' => '120000',
            'number' => '700',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'Galaxy S10+',
            'retail_name' => 'Samsung',
            'price' => '120000',
            'number' => '700',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'Galaxy S10+ 5G',
            'retail_name' => 'Samsung',
            'price' => '150000',
            'number' => '700',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'Galaxy fold',
            'retail_name' => 'Samsung',
            'price' => '210000',
            'number' => '200',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'Galaxy fold 5G',
            'retail_name' => 'Samsung',
            'price' => '240000',
            'number' => '200',
            'created_at' => now()
        ]);

        //ここからGoogle
        DB::table('transactions')->insert([
            'device_name' => 'Pixel',
            'retail_name' => 'Google',
            'price' => '80000',
            'number' => '2000',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'Pixel 2',
            'retail_name' => 'Google',
            'price' => '90000',
            'number' => '1600',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'Pixel 2 XL',
            'retail_name' => 'Google',
            'price' => '110000',
            'number' => '800',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'Pixel 3',
            'retail_name' => 'Google',
            'price' => '90000',
            'number' => '1600',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'Pixel 3 XL',
            'retail_name' => 'Google',
            'price' => '110000',
            'number' => '800',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'Pixel 4',
            'retail_name' => 'Google',
            'price' => '90000',
            'number' => '2500',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'Pixel 4 XL',
            'retail_name' => 'Google',
            'price' => '110000',
            'number' => '1000',
            'created_at' => now()
        ]);

        //ここからHuawei
        DB::table('transactions')->insert([
            'device_name' => 'P30 Lite',
            'retail_name' => 'Huawei',
            'price' => '30000',
            'number' => '1000',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'P30',
            'retail_name' => 'Huawei',
            'price' => '60000',
            'number' => '1000',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'P30 Pro',
            'retail_name' => 'Huawei',
            'price' => '90000',
            'number' => '1200',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'MATE30 Lite',
            'retail_name' => 'Huawei',
            'price' => '30000',
            'number' => '1000',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'MATE30',
            'retail_name' => 'Huawei',
            'price' => '70000',
            'number' => '1000',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'MATE30 Pro',
            'retail_name' => 'Huawei',
            'price' => '100000',
            'number' => '1000',
            'created_at' => now()
        ]);

        //ここからASUS
        DB::table('transactions')->insert([
            'device_name' => 'Zenfone 6',
            'retail_name' => 'ASUS',
            'price' => '75000',
            'number' => '1000',
            'created_at' => now()
        ]);

        //ここからSONY
        DB::table('transactions')->insert([
            'device_name' => 'Xperia 1',
            'retail_name' => 'SONY',
            'price' => '90000',
            'number' => '1000',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'Xperia 5',
            'retail_name' => 'SONY',
            'price' => '90000',
            'number' => '1000',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'Xperia 8',
            'retail_name' => 'SONY',
            'price' => '90000',
            'number' => '1000',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'Xperia Ace',
            'retail_name' => 'SONY',
            'price' => '90000',
            'number' => '1000',
            'created_at' => now()
        ]);

        //ここから再注文
        DB::table('transactions')->insert([
            'device_name' => 'iPhone11',
            'retail_name' => 'docomo',
            'price' => '82000',
            'number' => '1200',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'iPhone11 Pro',
            'retail_name' => 'docomo',
            'price' => '120000',
            'number' => '900',
            'created_at' => now()
        ]);

        DB::table('transactions')->insert([
            'device_name' => 'Pixel 4',
            'retail_name' => 'SoftBank',
            'price' => '90000',
            'number' => '2500',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'Pixel 4 XL',
            'retail_name' => 'SoftBank',
            'price' => '110000',
            'number' => '1000',
            'created_at' => now()
        ]);

        DB::table('transactions')->insert([
            'device_name' => 'Pixel 4',
            'retail_name' => 'Google',
            'price' => '90000',
            'number' => '2500',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'Pixel 4 XL',
            'retail_name' => 'Google',
            'price' => '110000',
            'number' => '1000',
            'created_at' => now()
        ]);

        DB::table('transactions')->insert([
            'device_name' => 'iPhone11',
            'retail_name' => 'Apple',
            'price' => '82000',
            'number' => '1200',
            'created_at' => now()
        ]);
        DB::table('transactions')->insert([
            'device_name' => 'iPhone11 Pro',
            'retail_name' => 'Apple',
            'price' => '120000',
            'number' => '900',
            'created_at' => now()
        ]);

        DB::table('transactions')->insert([
            'device_name' => 'P30 Lite',
            'retail_name' => 'Huawei',
            'price' => '30000',
            'number' => '1000',
            'created_at' => now()
        ]);

    }
}
