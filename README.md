# transaction_matching for laravel  

***起動方法***  
docker-laravelのビルドと起動  
```  
cd transaction_matching_for_laravel  
docker-compose build  
docker-compose up -d  
docker-compose exec app ash  
```  
ここでコンテナ(VM)の中に入ります  
```  
php artisan migrate:fresh --seed  
```  
ここまででDBの準備は完了．  
次に「transaction」テーブルと「sales」テーブルを同期します．  
```
php artisan command:sync  
```
あとは，matchingと「sales」テーブルへの反映を実行します．  
```  
php artisan command:matching  
```  
このあと，DBの「matching」テーブルにマッチング結果が出力されます． 
また，「sales」テーブルにも値下げ後の価格が反映されています．
  
  
***DBについて***  
接続の設定は以下の通り(.envファイルを参照・適宜変更してください)  
```  
Name    : transaction_match(任意の名前で可)  
Ver.    : MySQL 8.x  
Host    : 127.0.0.1  
Port    : 13306  
User    : homestead  
Pass    : secret  
DB_name : homestead  
```  
各テーブルの説明  
・transaction : 元々の取引のデータ(値下げ前などの取引ログも残る)  
・invoice : 値引きの申請データ  
・matching : 上記２つのテーブルのidの対応づけを表す(値引き後の価格も載せている)  
・sales : 実際の売り上げを表したもの(ここを見れば値下げ後の売り上げもわかる)  
  
  
***停止方法について***  
使用後はコンテナ内で  
```  
exit  
```  
すると．/transaction_matching_for_laravelに戻ってくるので  
```  
docker-compose down  
```  